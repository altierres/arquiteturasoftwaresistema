<!DOCTYPE html>
<html lang="pt-br">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PedidosNOW</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/site/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('vendor/site/css/scrolling-nav.css') }}" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}">Pedidos NOW</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          @if (Route::has('login'))
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger carrinhoLista" id="carrinhoLista" ><i class="fa fa-shopping-cart"> Carrinho</i></a>
            </li>
            @auth
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ url('/') }}">Site</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ url('/admin') }}">ADMIN</a>
              </li>
            @else
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Entrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
            @endauth
          </ul>
          @endif

        </div>
      </div>
    </nav>

    <header class="bg-primary text-white">
      <div class="container text-center">
        <h1>Vamos realizar um Pedido?</h1>
        <p class="lead">Escolha os produtos que deseje comprar.</p>
      </div>
    </header>

      <div class="row">

        @forelse($produtos as $produto)
          <div class="col-sm-3 col-xs-3 col-lg-3">
            <div class="painel-produto">
              <center>

                <img class="profile-user-img img-responsive img-circle" src="https://img.icons8.com/windows/96/000000/vegan-clothing.png" alt="">
                <br>
                <h3 class="profile-username text-primary text-center">{{$produto->name}}</h3>
                <p class="text-muted text-center">R$ {{ number_format($produto->sale_value, 2, ',', '.' ) }}</p>
                
                <button onclick="addProduto({{$produto->id}})" class="btn btn-primary" id="btnCompra{{$produto->id}}" name="  "><i class="fa fa-shopping-cart"> Comprar</i></button>

              </center>
              <br>
            </div>
          </div>
        @empty
          <p>Não foram encontrados registros</p>
        @endforelse

      </div>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; PedidosNOW</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/adminlte/vendor/jquery/dist/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="js/scrolling-nav.js"></script>


  </body>

    <script>

      var listaCompras = [];

      // Função de adição de produtos
      function addProduto(id) {
        //Lista de compras
        listaCompras.push(id);
        //Reculpera ID
        var botao = '#btnCompra'+id;
        //Desativa botão após adicinar
        $(botao).attr("disabled",true);
        //Modifica o texto
        $(botao).text("Adicionado");
      }


      /*
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
      */

      $(document).ready(function(){
          //{!! URL::to('admin/categories/" +idtoedit +"')!!}
          $(".carrinhoLista").click(function(){
            if(listaCompras.length == 0){
              //Carrinho vazio - não fazer
              //window.location = "carrinho/-1";

            }else{
              //Criado json de pedidos
              var myJsonString = JSON.stringify(listaCompras);
              //Redireciona para o carrinho de compras com lista de produtos
              window.location = "carrinho/"+ myJsonString;
            }
            

            //var url = 'carrinho/' + listaCompras;
            //var url = "{!! URL::to('carrinho/" + listaCompras +"')!!}";
            // $.get(url,function(data){
            //  console.log(data);
           //   alert(data);
            // });


          });
            
      });

    </script>

</html>