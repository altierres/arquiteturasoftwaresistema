<!DOCTYPE html>
<html lang="pt-br">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PedidosNOW</title>

    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/site/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('vendor/site/css/scrolling-nav.css') }}" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}"	>Pedidos NOW</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          @if (Route::has('login'))
          <ul class="navbar-nav ml-auto">
            @auth
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ url('/') }}">Loja</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ url('/admin') }}">ADMIN</a>
              </li>
            @else
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Entrar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{ route('register') }}">Registrar</a>
              </li>
            @endauth
          </ul>
          @endif

        </div>
      </div>
    </nav>

    <header class="bg-primary text-white">
      <div class="container text-center">
        <h3>Carrinho de compras</h3>
        <h1> 
	        <small>Total: </small>
	        R$ {{ number_format($totalPedido, 2, ',', '.' ) }}
	     </h1>
      </div>
    </header>
    <br>
     <div class="container" >
     	<h2>Produtos</h2>
    	<table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-md-4">Nome</th>
                        <th class="col-md-2">Descrição</th>
                        <th class="col-md-2">Valor</th>
                    <tr>
                </thead>
                <tbody>
                    @forelse($produtos as $produto)
                        
                            <tr>
                                <td> {{ $produto->name }} </td>
                                <td> {{ $produto->description }} </td>
                                <td> R$ {{ number_format($produto->sale_value, 2, ',', '.' ) }} </td>
                            </tr>                        
                    @empty
                        <p>Não foram encontrados registros</p>
                    @endforelse
                </tbody>      
            </table>		
      </div>
    <div class="container">
    	<h2>Endereço de entrega</h2>
      <div>
        @if ($errors->any())
          <div class="alert alert-warning">
            @foreach ( $errors->all() as $error )
              <p> {{ $error }} </p>
            @endforeach
          </div>
        @endif
      </div>
    	<form action="{{ route('carrinho.finalizar') }}"  method="post">
	    	{!! csrf_field() !!}

	    	<input type="hidden" name="produtos" value="{{ $produtos }}" >
	    	<input type="hidden" name="totalPedido" value="{{ $totalPedido }}" >

	        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label for="cep">CEP</label>
                  <input type="text" class="form-control cep" id="cep" name="cep" placeholder="00000-000" value="{{ $fornecedor->endereco->cep or old('cep')}}" >
                  <input type="button" value="Pesquisar CEP" class="submit btn btn-warning pull-right btnpesquisarcep" id="btnpesquisarcep">
              </div>
            </div>

	          <div class="col-md-10">
	            <div class="form-group">
	              <label for="street">Rua</label>
	              <input type="text" class="form-control street" id="street" name="street" placeholder="Informe o nome da rua..." value="{{ $fornecedor->endereco->street or old('street') }}"  disabled>
	              <p class="help-block">Ex: Avenida Brasil</p>
	            </div>
	          </div>

	          <div class="col-md-2">
	            <div class="form-group">
	              <label for="number">Número</label>
	              <input type="text" class="form-control number" id="number" name="number" placeholder="Informe o numero..." value="{{ $fornecedor->endereco->number or old('number') }}"  disabled>
	            </div>
	          </div>                                                
	        </div>

	        <div class="row">
	          <div class="col-md-7">
	            <div class="form-group">
	              <label for="city">Cidade</label>
	              <input type="text" class="form-control city" id="city" name="city" placeholder="Informe a cidade..." value="{{ $fornecedor->endereco->city or old('city')}}"  disabled>
	            </div>
	          </div>

	          <div class="col-md-5">
	            <div class="form-group">
	              <label for="state">Estado</label>
                  <select id="state" name="state" class="form-control state"  disabled>
	      			      <option value="" > -- Selecione o estado -- </option>
	      			                    @foreach ($estados as $estado)
	      			      <option value="{{ $estado }}"
	                    @if( (isset($fornecedor) && $fornecedor->endereco->state) or old('state') == $estado)
	                                        selected
	                                      @endif
	                                    > {{$estado}} </option>
	      			        @endforeach
	      			    </select>
	            </div>
	          </div>	                        
	        </div>

	        <input type="submit" value="Finalizar Pedido" class="submit btn btn-primary pull-right">
	        <br>
	        <br>
	   	</form>	
    </div>

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; PedidosNOW</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/adminlte/vendor/jquery/dist/jquery.min.js"></script>

    <script src="{{ asset('js/jquery.mask.min.js') }}"></script>

  </body>

	
  <script>
		$('.cep').mask('00000-000');
    
    $(document).ready(function(){

      $('#btnpesquisarcep').click(function(){
        var cep = $('.cep').val();
        if(cep == null || cep == ''){
          alert("Informe o cep de sua cidade ou endereço!");
        }else{
          //pesquisar CEP
          //Habilitar campos
          habilitarCamposEndereco();

          //Requisição GET para pesquisa CEP
          var url = "https://viacep.com.br/ws/" + cep + "/json/";
          $.ajax({
              url: url,
              type: 'GET',
              //dataType: 'json', // added data type
              success: function(retorno) {
                if(retorno.erro){
                  alert("Não foi possível pesquisar pelo cep informado, por favor informe manualmente o endereço de entraga.");
                }else{
                  $('.street').val(retorno.logradouro);
                  $('.city').val(retorno.localidade);
                  setEstado(retorno.uf);
                }                
              },
              error: function (retorno) {
                  alert("Não foi possível pesquisar pelo cep informado, por favor informe manualmente o endereço de entraga.");
              }
          });
          
        }

      });

    });

    function habilitarCamposEndereco(){
      $('.street').prop('disabled', false);
      $('.number').prop('disabled', false);
      $('.city').prop('disabled', false);
      $('.state').prop('disabled', false);      
    }

    function setEstado(estado){
      if(estado == "MG"){
        $(".state").val("Minas Gerais");
      }else if(estado == "SP"){
        $(".state").val("São Paulo");
      }else if(estado == "RJ"){
        $(".state").val("Rio de Janeiro");
      }
    }

  </script>



</html>