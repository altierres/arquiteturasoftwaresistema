@if ($errors->any())
    <div class="alert alert-warning">
    	@foreach ($errors->all() as $error)
    		<p>{{ $error }}</p>    					
    	@endforeach
    </div> 
@endif

@if (session('sucesso'))
	<div class="alert alert-success" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	  <strong>Sucesso!</strong> {{ session('sucesso') }}
	</div>
@endif

@if (session('error'))
	<div class="alert alert-danger">
	  <strong>Erro!</strong> {{ session('error') }}
	</div>	
@endif