@extends('adminlte::page')

@section('title', 'Admin Pedidos NOW')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
  @can('admin')
    <div class="row">

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$totalFornecedor}}</h3>

              <p>Total de Fornecedores</p>
            </div>
            <div class="icon">
              <i class="ion ion-plane"></i>
            </div>
            <a href="{{ route('fornecedores.index') }}" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$totalProdutos}}</h3>

              <p>Total de Produtos</p>
            </div>
            <div class="icon">
              <i class="ion ion-tshirt"></i>
            </div>
            <a href="{{ route('produtos.index') }}" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$totalUsuarios}}</h3>

              <p>Usuarios Registrados</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$totalPedidos}}</h3>

              <p>Total de Pedidos</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-cart"></i>
            </div>
            <a href="{{ route('pedidos.index') }}" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{$pedidosAbertos}}</h3>

              <p>Pedidos em Aberto</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-flower"></i>
            </div>
            <a href="{{ route('pedidos.pendentes') }}" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

  	</div>
  @endcan

    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <h3>Meus Pedidos</h3>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$meusPedidos}}</h3>

              <p>Meus Pedidos</p>
            </div>
            <div class="icon">
              <i class="ion ion-briefcase"></i>
            </div>
            <a href="{{ route('meuspedidos') }}" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div>
@stop

