@extends('adminlte::page')

@section('title', 'Dados Pedido')

@section('content_header')
    <h1>
        Pedido 
        <small>Dados</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li><a href="{{route('fornecedores.index')}}">Pedido</a></li>
    	<li>Informações</li>        
    </ol>
@stop

@section('content')
	
	@include('includes/alertas')

   <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dados Gerais</h3>
              <div class="box-tools pull-right">
                <a href="" class="btn btn-default btn-group"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

                <a href="")}}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i>Atualizar</a>
              </div>
            </div>
            <!-- /.box-header -->
            <form class="form-horizontal">
              <div class="box-body">
                
                <table class="table table-bordered">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Cliente</th>
                      </tr>
                      <th class="warning" colspan="1">Nome</th>
                      <td colspan="2">{{$pedido->usuario->name}}</td>
                      <th class="warning" colspan="1">Email</th>
                      <td colspan="2">{{$pedido->usuario->email}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Pedido</th>
                      </tr>
                      <th class="warning" colspan="1">Data</th>
                      <td colspan="2">{{$pedido->date}}</td>
                      <th class="warning" colspan="1">Total Pedido</th>
                      <td colspan="2">{{$pedido->total_final}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Situação Pedido</th>
                      <td colspan="2">{{$pedido->state}}</td>
                      <th class="warning" colspan="1">Situação Pagamento</th>
                      <td colspan="2">{{$pedido->state_sale}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Endereço de Entrega</th>
                      </tr>
                      <th class="warning" colspan="1">Rua</th>
                      <td colspan="3">{{$pedido->endereco->street}}</td>
                      <th class="warning" colspan="1">Número</th>
                      <td colspan="1">{{$pedido->endereco->number}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Cidade</th>
                      <td colspan="1"> {{$pedido->endereco->city}} </td>
                      <th class="warning" colspan="1">Estado</th>
                      <td colspan="1"> {{$pedido->endereco->state}} </td>
                      <th class="warning" colspan="1">CEP</th>
                      <td colspan="1"> {{$pedido->endereco->cep}} </td>
                    </tr>
                  </tbody>
                </table>

              </div>
              
              <div class="box-footer">
                <h2>Produtos</h2>

                <table class="table table-bordered table-hover">
                  <thead>
                      <tr class="info">
                        <th >Imagem</th>
                        <th >Produto</th>
                        <th >Valor Unitário</th>
                        <th >Quantidade</th>
                        <th >SubTotal</th>
                      <tr>
                  </thead>
                  <tbody>
                    @forelse($carrinhos as $carrinho)
                      <tr>
                        <td> {{ $carrinho->produto->imagem }} </td>
                        <td> {{ $carrinho->produto->name }} </td>
                        <td> {{ $carrinho->produto->sale_value }} </td>
                        <td> {{ $carrinho->amount }} </td>
                        <td> {{ $carrinho->total }} </td>
                      </tr>
                        
                    @empty
                        <p>Não foram encontrados registros</p>
                    @endforelse
                  </tbody>      
                </table>
                
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

@stop
