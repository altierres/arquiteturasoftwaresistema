@extends('adminlte::page')

@section('title', 'Dados Pedido')

@section('content_header')
    <h1>
        Pedido 
        <small>Dados</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li><a href="{{route('fornecedores.index')}}">Pedido</a></li>
    	<li>Informações</li>        
    </ol>
@stop

@section('content')
	
	@include('includes/alertas')

   <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dados Gerais</h3>
              <div class="box-tools pull-right">
                <a href="" class="btn btn-default btn-group"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

                
              </div>
            </div>
            <!-- /.box-header -->
          
              <div class="box-body">
                
                <table class="table table-bordered">
                  <thead>

                    <form action="{{ route('pedidos.update', $pedido->id) }}"  method="post">
                      {!! method_field('PATCH') !!}
                      {!! csrf_field() !!}
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="state">Situação Pedido</label>

                          <select id="state" name="state" class="form-control">
                            <option value="" > -- Selecione o Situação -- </option>
                            <option value="Aprovado" 
                              @if( (isset($pedido) && $pedido->state == 'Aprovado' ) ) 
                                selected 
                              @endif >Aprovado</option>
                            <option value="Cancelado"
                              @if( (isset($pedido) && $pedido->state == 'Cancelado' ) ) 
                                selected 
                              @endif >Cancelado</option>
                            <option value="Aguardando" 
                              @if( (isset($pedido) && $pedido->state == 'Aguardando' ) ) 
                                selected 
                              @endif >Aguardando</option>
                            <option value="Encaminhado" 
                              @if( (isset($pedido) && $pedido->state == 'Encaminhado' ) ) 
                                selected 
                              @endif >Encaminhado</option>
                            <option value="Entregue" 
                              @if( (isset($pedido) && $pedido->state == 'Entregue' ) ) 
                                selected 
                              @endif  >Entregue</option>   
                          </select>

                        </div>
                      </div>
                      <div class="col-md-5">
                        <div class="form-group">
                          <label for="state_sale">Situação Pagamento</label>

                          <select id="state_sale" name="state_sale" class="form-control">
                            <option value="" > -- Selecione o estado -- </option>
                            <option value="Confirmado" 
                              @if( (isset($pedido) && $pedido->state_sale == 'Confirmado' )  ) 
                                selected 
                              @endif >Confirmado</option>
                            <option value="Cancelado" 
                              @if( (isset($pedido) && $pedido->state_sale == 'Cancelado' )  ) 
                                selected 
                              @endif >Cancelado</option>
                            <option value="Aguardando" 
                              @if( (isset($pedido) && $pedido->state_sale == 'Aguardando' )  ) 
                                selected 
                              @endif >Aguardando</option>  
                          </select>

                          

                        </div>
                      </div>
                      <div class="col-md-1">
                        <div class="form-group">
                          <label for="state">----</label>
                          <input type="submit" value="Atualizar" class="submit btn btn-warning pull-right">
                        </div>
                      </div>
                    </form>
                    
                  </thead>
                  <tbody>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Cliente</th>
                      </tr>
                      <th class="warning" colspan="1">Nome</th>
                      <td colspan="2">{{$pedido->usuario->name}}</td>
                      <th class="warning" colspan="1">Email</th>
                      <td colspan="2">{{$pedido->usuario->email}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Pedido</th>
                      </tr>
                      <th class="warning" colspan="1">Data</th>
                      <td colspan="2">{{$pedido->getDataAttribute($pedido->date)}}</td>
                      <th class="warning" colspan="1">Total Pedido</th>
                      <td colspan="2">R$ {{ number_format($pedido->total_final, 2, ',', '.' ) }}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Endereço de Entrega</th>
                      </tr>
                      <th class="warning" colspan="1">Rua</th>
                      <td colspan="3">{{$pedido->endereco->street}}</td>
                      <th class="warning" colspan="1">Número</th>
                      <td colspan="1">{{$pedido->endereco->number}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Cidade</th>
                      <td colspan="1"> {{$pedido->endereco->city}} </td>
                      <th class="warning" colspan="1">Estado</th>
                      <td colspan="1"> {{$pedido->endereco->state}} </td>
                      <th class="warning" colspan="1">CEP</th>
                      <td colspan="1"> {{$pedido->endereco->cep}} </td>
                    </tr>
                  </tbody>
                </table>

              </div>
              
              <div class="box-footer">
                <h2>Produtos</h2>

                <table class="table table-bordered table-hover">
                  <thead>
                      <tr class="info">
                        <th >Imagem</th>
                        <th >Produto</th>
                        <th >Valor Unitário</th>
                        <th >Quantidade</th>
                        <th >SubTotal</th>
                      <tr>
                  </thead>
                  <tbody>
                    @forelse($carrinhos as $carrinho)
                      <tr>
                        <td> {{ $carrinho->produto->imagem }} </td>
                        <td> {{ $carrinho->produto->name }} </td>
                        <td> R$ {{ number_format($carrinho->produto->sale_value, 2, ',', '.' ) }} </td>
                        <td> {{ $carrinho->amount }} </td>
                        <td> R$ {{ number_format($carrinho->total, 2, ',', '.' ) }} </td>
                      </tr>
                        
                    @empty
                        <p>Não foram encontrados registros</p>
                    @endforelse
                  </tbody>      
                </table>
                
              </div>
              <!-- /.box-footer -->
            
          </div>
          <!-- /.box -->

@stop
