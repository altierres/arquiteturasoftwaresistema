@extends('adminlte::page')

@section('title', 'Novo Produto')

@section('content_header')
    <h1>
        Produtos 
        <small>Novo</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>    	
        <li><a href="{{route('produtos.index')}}"}>Produtos</a></li>
        <li>Cadastro</li>
    </ol>
@stop

@section('content')

    <div class="box">
    	
    	<div class="box-body">

    		@include('includes/alertas')

        @if( isset($produto) )
          <form action="{{ route('produtos.update', $produto->id) }}"  method="post">
          {!! method_field('PATCH') !!}
          
        @else
          <form action="{{route('produtos.store')}}" method="post" enctype="multipart/form-data">
        @endif

                {!! csrf_field() !!}

                <div class="row">
                  <div class="col-md-12">
                    <div class="box box-primary">
                      <div class="box-body">
                        <!-- Centered Tabs -->
                        <ul class="nav nav-tabs nav-justified">
                          <li class="active"><a href="#home">Dados do Produto</a></li>
                          <li><a href="#menu2">Valores</a></li>
                          <li><a href="#menu4">Fotos</a></li>
                        </ul>

                        <div class="tab-content">
                          <div id="home" class="tab-pane fade in active">
                            <br>
                              <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">Nome</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Informe nome..." value="{{ $produto->name or old('name') }}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="provider">Fornecedor</label>

		                                <select id="provider" name="provider_id" class="form-control">
		      			                    <option value="" > -- Selecione o fornecedor -- </option>
		      			                    @foreach ($fornecedores as $fornecedor)
		      			                    <option value="{{ $fornecedor->id }}"
		                                      @if( (isset($produto) && $produto->provider_id) or old('provider') == $fornecedor->id)
		                                        selected
		                                      @endif
		                                    > {{$fornecedor->name}} </option>
		      			                    @endforeach
		      			                </select>

                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="category">Categoria</label>
                                        <select id="category" name="category_id" class="form-control">
		      			                    <option value="" > -- Selecione a cetegoria -- </option>
		      			                    @foreach ($categorias as $categoria)
		      			                    <option value="{{ $categoria->id }}"
		                                      @if( (isset($produto) && $produto->category_id) or old('category') == $categoria->id)
		                                        selected
		                                      @endif
		                                    > {{$categoria->name}} </option>
		      			                    @endforeach
		      			                </select>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row">                
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="description">Descrição</label>
                                        <textarea class="form-control" rows="2" id="description" name="description" placeholder="Informe a descrição do produto..."> {{ $produto->description or old('description') }}</textarea>
                                    </div>
                                </div>
                            </div>

                          </div>

                        <div id="menu2" class="tab-pane fade">
                            <br>
                              <div class="row">

                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="valor_compra">Preço de Compra</label>
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input class="form-control dinheiro" step="1" placeholder="0.00 " id="valor_compra" name="purchase_value" type="number" value="{{ $produto->purchase_value or old('puchase_value') }}">
                                        <span class="input-group-addon">por unidade</span>
                                      </div>                      
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label for="valor_venda">Valor de Venda</label>
                                      <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input class="form-control dinheiro" step="1" placeholder="0.00" id="valor_venda" name="sale_value" type="number" value="{{ $produto->sale_value or old('sale_value') }}">
                                        <span class="input-group-addon">por unidade</span>
                                      </div>                      
                                    </div>
                                  </div>

                              </div>
                          </div>

                          <div id="menu4" class="tab-pane fade">
                            <br>
                              <div class="row" >
                                <div class="col-md-12">
                                  <div class="input-group" >
                                     <label>Fotos apenas no formato .PNG ou .JPG</label>     
                                  </div>         
                                </div>
                              </div>
                              <div class="row" >
                                <div class="col-md-12">
                                  <div class="input-group" >
                                    <img id="blah" src="http://placehold.it/300" alt="your image" />     
                                  </div>         
                                </div>
                              </div>

                              <div class="row " >
                                <div class="col-md-12">
                                  <div class="input-group">
                                    <input name='' id="" type='file' class="form-control-file" onchange="readURL(this);" />  
                                  </div>                        
                                </div>
                              </div>

                          </div>

                        </div>

                      </div>

                      <div class="box-footer">
                        <input type="submit" value="Salvar" class="submit btn btn-primary pull-right">
                      </div>
                    </div>
                  </div>
                </div>

              </form>

    	</div>
    </div>
@stop

@section('css')
  <style type="text/css">
    
    img {
      max-width:300px;
    } 
    
  </style>

@stop

@section('js')
    <script>

        // Responsável por controler os TABS do Form
          $(document).ready(function(){
              $(".nav-tabs a").click(function(){
                  $(this).tab('show');
              });      
          });

          //Responsável por carregar as fotos
          function readURL(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#blah').attr('src', e.target.result);
              };

              reader.readAsDataURL(input.files[0]);
            }
          }

          //$('.dinheiro').mask('#.##0,00', {reverse: true});

    </script>
@stop



