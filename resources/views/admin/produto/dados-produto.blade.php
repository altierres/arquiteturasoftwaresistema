@extends('adminlte::page')

@section('title', 'Dados Produto')

@section('content_header')
    <h1>
        Produto 
        <small>Dados</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li><a href="{{route('produtos.index')}}">Produtos</a></li>
    	<li>Informações</li>        
    </ol>
@stop

@section('content')
	
	@include('includes/alertas')

   <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dados Gerais</h3>

              <div class="box-tools pull-right">
                <a href="{{ route('produtos.index') }}" class="btn btn-default btn-group"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

                <a href="{{ route('produtos.show',$produto->id) }}" class="btn btn-danger btn-group"><i class="fa fa-trash" aria-hidden="true"></i> Excluir</a>
                
                <a href="{{ route('produtos.edit',$produto->id) }}")}}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i>Atualizar</a>
              </div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">
                
                <table class="table table-bordered">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr>
                      <tr class="info">
                        <th colspan="10">Dados do Produto</th>
                      </tr>
                      <th class="warning" colspan="1">Nome</th>
                      <td colspan="3">{{$produto->name}}</td>
                      <th class="warning" colspan="1">Categoria</th>
                      <td colspan="3">{{$produto->categoria->name}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Descrição</th>
                      <td colspan="9">{{$produto->description}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="10">Dados de Valores</th>
                      </tr>
                      <th class="warning" colspan="2">Preço de Compra</th>
                      <td colspan="3">R$ {{ number_format($produto->purchase_value, 2, ',', '.' ) }}</td>
                      <th class="warning" colspan="2">Preço de Venda</th>
                      <td colspan="3">R$ {{ number_format($produto->sale_value, 2, ',', '.' ) }}</td>
                    </tr>
                  </tbody>
                </table>

              </div>

            </form>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Fornecedor</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            
            <div class="box-body" style="">

                <table class="table table-bordered">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Fornecedor</th>
                      </tr>
                      <th class="warning" colspan="1">Nome</th>
                      <td colspan="2">{{$produto->fornecedor->name}}</td>
                      <th class="warning" colspan="1">CNPJ</th>
                      <td colspan="2">{{$produto->fornecedor->cnpj}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados de Endereço</th>
                      </tr>
                      <th class="warning" colspan="1">Rua</th>
                      <td colspan="3">{{$produto->fornecedor->endereco->street}}</td>
                      <th class="warning" colspan="1">Número</th>
                      <td colspan="1">{{$produto->fornecedor->endereco->number}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Cidade</th>
                      <td colspan="1"> {{$produto->fornecedor->endereco->city}} </td>
                      <th class="warning" colspan="1">Estado</th>
                      <td colspan="1"> {{$produto->fornecedor->endereco->state}} </td>
                      <th class="warning" colspan="1">CEP</th>
                      <td colspan="1"> {{$produto->fornecedor->endereco->cep}} </td>
                    </tr>
                  </tbody>
                </table>           
              
            </div>
          </div>
        </div>


@stop
