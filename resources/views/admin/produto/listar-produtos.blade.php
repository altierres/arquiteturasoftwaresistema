@extends('adminlte::page')

@section('title', 'Produtos')

@section('content_header')
    <h1>
        Produtos 
        <small>Lista</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li>Lista</li>
        <li>Produtos</li>
    </ol>
@stop

@section('content')
    
    

    <div class="row">
        <form action="" method="post">
            <div class="col-md-12">
                <div class="box box-default collapsed-box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesquisa Avançada</h3>
                        <div class="box-tools pull-right">
                          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                    </div>

                <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="nome_pesquisa">Nome</label>
                          <input type="text" class="form-control" id="nome_pesquisa" name="nome_pesquisa" placeholder="Pesquisar por nome...">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="cnpj_pesquisa">Código</label>
                          <input type="text" class="form-control" id="cnpj_pesquisa" name="cnpj_pesquisa" placeholder="Pesquisar por CNPJ...">
                        </div>
                      </div>
                      <div class="col-md-2">
                        <button type="submit" class="btn btn-success btn-block" style="margin-top: 17%">Pesquisar</button>
                      </div>
                    </div>
                </div>
              
                </div> 
            </div>
        </form>
    </div>


    <div class="box">

        <div class="box-header">
            <div class="container-fluid">
        <div class="row">            
            <a href="{{ route('produtos.create') }}" class="btn btn-primary btn-lg pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Novo Produto</a>
        </div>
    </div>
        </div>
    	
    	<div class="box-body">

    		@include('includes/alertas')

    		<table class="table table-bordered table-hover">
                <thead>
                    <tr class="info">
                        <th >Imagem</th>
                        <th >Nome</th>
                        <th >Descrição</th>
                        <th >Valor Compra</th>
                        <th >Valor Venda</th>
                        <th >Ações</th>
                    <tr>
                </thead>
                <tbody>
                    @forelse($produtos as $produto)
                        <tr>
                            <td> {{ $produto->imagem }} </td>
                            <td> {{ $produto->name }} </td>
                            <td> {{ $produto->description }} </td>
                            <td>R$ {{ number_format($produto->purchase_value, 2, ',', '.' ) }} </td>
                            <td>R$ {{ number_format($produto->sale_value, 2, ',', '.' ) }} </td>                            
                            <td style="text-align: right;" >
                              <a href="{{ route('produtos.show',$produto->id) }}" class="btn btn-info btn-group"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a href="{{ route('produtos.edit',$produto->id) }}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @empty
                        <p>Não foram encontrados registros</p>
                    @endforelse
                </tbody>      
            </table>

    	</div>
    </div>
@stop