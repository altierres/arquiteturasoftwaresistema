@extends('adminlte::page')

@section('title', 'Pedidos')

@section('content_header')
    <h1>
        Meus Pedidos 
        <small>Lista</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li>Lista</li>
        <li>Meus Pedidos</li>
    </ol>
@stop

@section('content')
    
    <div class="row">
            <form action="" method="post">
                <div class="col-md-12">
                    <div class="box box-default collapsed-box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">Pesquisa Avançada</h3>
                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                    <div class="box-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="nome_pesquisa">Nome</label>
                              <input type="text" class="form-control" id="nome_pesquisa" name="nome_pesquisa" placeholder="Pesquisar por nome...">
                            </div>
                          </div>
                          <div class="col-md-4">
                            <div class="form-group">
                              <label for="cnpj_pesquisa">CNPJ</label>
                              <input type="text" class="form-control" id="cnpj_pesquisa" name="cnpj_pesquisa" placeholder="Pesquisar por CNPJ...">
                            </div>
                          </div>
                          <div class="col-md-2">
                            <button type="submit" class="btn btn-success btn-block" style="margin-top: 17%">Pesquisar</button>
                          </div>
                        </div>
                    </div>
                  
                    </div> 
                </div>
            </form>
    </div>


    <div class="box">
    	
    	<div class="box-body">

    		@include('includes/alertas')

    		<table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="col-md-2">Data</th>
                        <th class="col-md-1">Total</th>
                        <th class="col-md-2">Situação</th>
                        <th class="col-md-2">Situação Pagamento</th>
                        <th class="col-md-2">Ação</th>
                    <tr>
                </thead>
                <tbody>
                    @forelse($pedidos as $pedido)
                        
                        <tr>
                            <td> {{ $pedido->getDataAttribute($pedido->date) }} </td>
                            <td> R$ {{ number_format($pedido->total_final, 2, ',', '.' ) }} </td>
                            <td> {{ $pedido->state }} </td>
                            <td> {{ $pedido->state_sale }} </td>
                            <td style="text-align: right;" >
                                       
                                <a href="{{ route('pedidos.show',$pedido->id) }}" class="btn btn-info btn-group"><i class="fa fa-eye" aria-hidden="true"></i> Ver</a>
                                        
                                <a href="{{ route('pedidos.edit',$pedido->id) }}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i> Atualizar</a>
                                    
                            </td>
                        </tr>
                        
                    @empty
                        <p>Não foram encontrados registros</p>
                    @endforelse
                </tbody>      
            </table>

    	</div>
    </div>
@stop