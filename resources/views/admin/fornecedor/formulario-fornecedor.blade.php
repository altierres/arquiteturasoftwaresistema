@extends('adminlte::page')

@section('title', 'Novo Fornecedor')

@section('content_header')
    <h1>
        Fornecedores 
        <small>Novo</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li><a href="{{route('fornecedores.index')}}">Fornecedor</a></li>
    	<li>Cadastro</li>        
    </ol>
@stop

@section('content')
	
	@include('includes/alertas')

    @if( isset($fornecedor) )
      <form action="{{ route('fornecedores.update', $fornecedor->id) }}"  method="post">
      {!! method_field('PATCH') !!}
      
    @else
      <form action="{{ route('fornecedores.store') }}"  method="post">
    @endif            

    	{!! csrf_field() !!}

        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Dados do Fornecedor</h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div>
              </div>

              <div class="box-body">
                <!-- Centered Tabs -->
                <ul class="nav nav-tabs nav-justified">
                  <li class="active"><a href="#empresa">Dados da Empresa</a></li>
                  <li><a href="#endereco">Dados de Endereço</a></li>
                </ul>

                <div class="tab-content">
                  <div id="empresa" class="tab-pane fade in active">
                    <br>
                      <div class="row">

                        <div class="col-md-8">
                            <div class="form-group">
                              <label for="name">Nome</label>
                              <input type="text" class="form-control" id="name" name="name" placeholder="Informe o nome..." value="{{ $fornecedor->name or old('name') }}" >
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                              <label for="cnpj">CNPJ</label>
                              <input type="text" class="form-control cnpj" id="cnpj" name="cnpj" placeholder="00.000.000/0000-00" value="{{ $fornecedor->cnpj or old('cnpj') }}" >
                            </div>
                        </div>
                        
                      </div>

                  </div>

                  <div id="endereco" class="tab-pane fade">
                    <br>
                      <div class="row">
                        <div class="col-md-10">
                          <div class="form-group">
                            <label for="street">Rua</label>
                            <input type="text" class="form-control" id="street" name="street" placeholder="Informe o nome da rua..." value="{{ $fornecedor->endereco->street or old('street') }}" >
                            <p class="help-block">Ex: Avenida Brasil</p>
                          </div>
                        </div>

                        <div class="col-md-2">
                          <div class="form-group">
                            <label for="number">Número</label>
                            <input type="text" class="form-control" id="number" name="number" placeholder="Informe o numero..." value="{{ $fornecedor->endereco->number or old('number') }}" >
                          </div>
                        </div>                                                
                      </div>

                      <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="city">Cidade</label>
                            <input type="text" class="form-control" id="city" name="city" placeholder="Informe a cidade..." value="{{ $fornecedor->endereco->city or old('city')}}" >
                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="state">Estado</label>

                            <select id="state" name="state" class="form-control">
      			                    <option value="" > -- Selecione o estado -- </option>
      			                    @foreach ($estados as $estado)
      			                        <option value="{{ $estado }}"
                                      @if( (isset($fornecedor) && $fornecedor->endereco->state) or old('state') == $estado)
                                        selected
                                      @endif
                                    > {{$estado}} </option>
      			                    @endforeach
      			                </select>

                          </div>
                        </div>

                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="cep">CEP</label>
                            <input type="text" class="form-control cep" id="cep" name="cep" placeholder="00000-000" value="{{ $fornecedor->endereco->cep or old('cep')}}" >
                          </div>
                        </div>
                      </div>
                  </div>                 
                </div>
              </div>

              <div class="box-footer">
                <input type="submit" value="Salvar" class="submit btn btn-primary pull-right">
              </div>
              
            </div>
          </div>
        </div>

      </form>
@stop

@section('js')
	<script src="{{ asset('js/jquery.mask.min.js') }}"></script>

  <script>

    // Responsável por controlar os TABS do Form
		$(document).ready(function(){
		  $(".nav-tabs a").click(function(){
		    $(this).tab('show');
		  });      
		});

		// Mascaras de campo
		$('.cnpj').mask('00.000.000/0000-00');
		$('.cep').mask('00000-000');
  </script>
@stop