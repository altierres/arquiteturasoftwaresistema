@extends('adminlte::page')

@section('title', 'Dados Fornecedor')

@section('content_header')
    <h1>
        Fornecedores 
        <small>Dados</small>
      </h1>
    <ol class="breadcrumb">
    	<li><a href="/admin">Dashboard</a></li>
    	<li><a href="{{route('fornecedores.index')}}">Fornecedores</a></li>
    	<li>Informações</li>        
    </ol>
@stop

@section('content')
	
	@include('includes/alertas')

   <!-- right column -->
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Dados Gerais</h3>
              <div class="box-tools pull-right">
                <a href="{{ route('fornecedores.index') }}" class="btn btn-default btn-group"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

                  <a href="{{ route('fornecedores.show',$fornecedor->id) }}" class="btn btn-danger btn-group"><i class="fa fa-trash" aria-hidden="true"></i> Excluir</a>

                  <a href="{{ route('fornecedores.edit',$fornecedor->id) }}")}}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i>Atualizar</a>
              </div>
            </div>
            <!-- /.box-header -->
            <form class="form-horizontal">
              <div class="box-body">
                
                <table class="table table-bordered">
                  <thead>
                    
                  </thead>
                  <tbody>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados do Fornecedor</th>
                      </tr>
                      <th class="warning" colspan="1">Nome</th>
                      <td colspan="2">{{$fornecedor->name}}</td>
                      <th class="warning" colspan="1">CNPJ</th>
                      <td colspan="2">{{$fornecedor->cnpj}}</td>
                    </tr>
                    <tr>
                      <tr class="info">
                        <th colspan="6">Dados de Endereço</th>
                      </tr>
                      <th class="warning" colspan="1">Rua</th>
                      <td colspan="3">{{$fornecedor->endereco->street}}</td>
                      <th class="warning" colspan="1">Número</th>
                      <td colspan="1">{{$fornecedor->endereco->number}}</td>
                    </tr>
                    <tr>
                      <th class="warning" colspan="1">Cidade</th>
                      <td colspan="1"> {{$fornecedor->endereco->city}} </td>
                      <th class="warning" colspan="1">Estado</th>
                      <td colspan="1"> {{$fornecedor->endereco->state}} </td>
                      <th class="warning" colspan="1">CEP</th>
                      <td colspan="1"> {{$fornecedor->endereco->cep}} </td>
                    </tr>
                  </tbody>
                </table>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <h2>Produtos Fornecidos</h2>

                <table class="table table-bordered table-hover">
                  <thead>
                      <tr class="info">
                        <th >Imagem</th>
                        <th >Nome</th>
                        <th >Descrição</th>
                        <th >Valor Compra</th>
                        <th >Valor Venda</th>
                        <th >Ações</th>
                      <tr>
                  </thead>
                  <tbody>
                        @forelse($fornecedor->produtos as $produto)
                          <tr>
                            <td> {{ $produto->imagem }} </td>
                            <td> {{ $produto->name }} </td>
                            <td> {{ $produto->description }} </td>
                            <td>R$ {{ number_format($produto->purchase_value, 2, ',', '.' ) }} </td>
                            <td>R$ {{ number_format($produto->sale_value, 2, ',', '.' ) }} </td>                            
                            <td style="text-align: right;" >
                              <a href="{{ route('produtos.show',$produto->id) }}" class="btn btn-info btn-group"><i class="fa fa-eye" aria-hidden="true"></i></a>
                              <a href="{{ route('produtos.edit',$produto->id) }}" class="btn btn-warning btn-group"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            </td>
                          </tr>
                        @empty
                            <p>Não foram encontrados registros</p>
                        @endforelse
                    </tbody>      
                </table>
                
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <!-- /.box -->

@stop
