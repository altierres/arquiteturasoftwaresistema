<?php

$this->group(['middleware' => ['auth'], 'namespace' => 'Admin', 'prefix' => 'admin'], function(){
	
	/***
	* Home Admin
	***/
	$this->get('/', 'AdminController@index')->name('Admin.home');

	/***
	*CRUD Fornecedor
	***/
	$this->resource('fornecedores', 'FornecedorController');

	/***
	*CRUD Produto
	***/
	$this->resource('produtos', 'ProdutoController');

	/***
	*CRUD Pedido
	***/
	$this->resource('pedidos', 'PedidoController');
	$this->get('pendentes', 'PedidoController@pedidosPendentes')->name('pedidos.pendentes');
	$this->get('meuspedidos', 'PedidoController@meusPedidos')->name('meuspedidos');	

	

});

/***
	*Rotas Site Web(Vitrine) 
	***/
$this->get('/', 'Site\SiteController@index');

Route::get('carrinho/{produtos}', 'Site\SiteController@carrinhoCompras')->name('carrinhoCompras');

/***
*PedidoFinalizar
***/
$this->post('carrinhoFinalizar/', 'Admin\PedidoController@carrinhoFinalizar')->name('carrinho.finalizar');

//Route::post('carrinhoFinalizar/', 'Site\SiteController@carrinhoFinalizar')->name('carrinho.finalizar1');

Auth::routes();

