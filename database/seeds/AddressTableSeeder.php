<?php

use Illuminate\Database\Seeder;
use App\Models\Address;

class AddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Address::create([
        	'street' => 'Av. Madalena',
        	'number' => '130',
        	'city'   => 'Japonvar',
        	'state'  => 'Minas Gerais',
        	'cep'	=> '10000-000'
        ]);

        Address::create([
        	'street' => 'R. Alemão',
        	'number' => '12',
        	'city'   => 'Januaria',
        	'state'  => 'Minas Gerais',
        	'cep'	=> '40099-980'
        ]);
    }
}
