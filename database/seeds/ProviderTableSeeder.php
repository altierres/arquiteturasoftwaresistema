<?php

use Illuminate\Database\Seeder;
use App\Models\Provider;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Provider::create([
        	'name' => 'Modas Femininas',
        	'cnpj' => '1000.0000/0001',
        	'address_id' => 2,	
        ]);
    }
}
