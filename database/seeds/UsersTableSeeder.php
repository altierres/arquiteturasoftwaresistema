<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Altierres Washington',
        	'cpf' => '025.200.961-44',
        	'email' => 'altierres.si@gmail.com',
        	'password' => bcrypt('123456'),
            'level' => 'admin',
        	'address_id' => 1,
        ]);
    }
}
