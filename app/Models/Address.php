<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $fillable = ['street','number','city','state','cep'];

	public function usuario(){
    	return $this->hasOne(User::class);
    }

    public function fornecedor(){
    	return $this->hasOne(Provider::class);
    }

    public function pedido(){
    	return $this->hasOne(Order::class);
    }
    
}
