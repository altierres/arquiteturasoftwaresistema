<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;
use Carbon\Carbon;

class Order extends Model
{

	protected $fillable = ['total_final','date','state','state_sale','user_id','address_id'];

    public function carrinhos(){
	    return $this->hasMany(Cart::class,'order_id');
	}

	public function endereco(){
    	return $this->belongsTo(Address::class,"address_id");
    }

    public function usuario(){
    	return $this->belongsTo(User::class,"user_id");
    }

    public function getDataAttribute($valor){
        return Carbon::parse($valor)->format('d/m/Y');
    }

    public function novoPedido(Array $formDados): Array{

        DB::beginTransaction();

        //Cria o endereço com base nos dados do formulário
        $retorno_endereco = Address::create([
            'street' => $formDados['street'],
            'number' => $formDados['number'],
            'city'   => $formDados['city'],
            'state'  => $formDados['state'],
            'cep'    => $formDados['cep'],
        ]);

        //Cria novo pedido
        $retorno_pedido = Order::create([
            'date'        => date('Ymd'),
            'total_final' => $formDados['totalPedido'],
            'state'       => 'Aguardando',
            'state_sale'  => 'Aguardando',
            'user_id'     => auth()->user()->id,
            'address_id'  => $retorno_endereco->id,
        ]);

        $array = json_decode($formDados['produtos']);
        $retorno_carrinho = true;
       
        for ($i = 0; $i < count($array) ; $i++) {
            
            $retorno_carrinho = Cart::create([
                'amount'     => 1,
                'total'      => $array[$i]->sale_value,
                'order_id'   => $retorno_pedido->id,
                'product_id' => $array[$i]->id,
            ]);

            if($retorno_carrinho)
                continue;
            else
                break;
        }        

        //Verifica se ambos foram inserido e  realiza commit, caso contrário realiza o roolback
        if($retorno_endereco && $retorno_pedido && $retorno_carrinho){

            DB::commit();

            return [
                'sucesso'  => true,
                'mensagem' => 'Pedido cadastrado com sucesso.'
            ];
        }

        DB::roolback();

        return [
            'sucesso'  => false,
            'mensagem' => 'Erro ao cadastrar novo pedido.'
        ];
    }

    public function atualizarSituacao(Array $formDados): Array{

        DB::beginTransaction();

        //Atualiza o fornecedor
        $retorno_pedido = $this->update($formDados);

        //Verifica se ambos foram atualizados e  realiza commit, caso contrário realiza o roolback
        if($retorno_pedido){

            DB::commit();

            return [
                'sucesso'  => true,
                'mensagem' => 'Pedido atualizado com sucesso.'
            ];
        }

        DB::roolback();

        return [
            'sucesso'  => false,
            'mensagem' => 'Erro ao atualizado o pedido.'
        ];
    }

}
