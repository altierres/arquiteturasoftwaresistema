<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

	protected $fillable = ['amount','total','order_id','product_id'];

    public function pedido(){
    	return $this->belongsTo(Order::class,"order_id");
    }

    public function produto(){
    	return $this->belongsTo(Product::class,"product_id");
    }
}
