<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{

	protected $fillable = [
                'name',
                'description',
                'imagem',
                'purchase_value',
                'sale_value',
                'provider_id',
                'category_id'];

    //Função responável pelo OneToMany inverso com Fonecedor
    public function fornecedor(){
    	return $this->belongsTo(Provider::class,"provider_id");
    }

    //Função responável pelo OneToMany inverso com Categoria
    public function categoria(){
    	return $this->belongsTo(Category::class,"category_id");
    }

    public function carrinhos(){
        return $this->hasMany(Cart::class,'product_id');
    }

    public function novoProduto(Array $formDados): Array{

        DB::beginTransaction();

        //Cria o endereço com base nos dados do formulário
        $retorno_produto = $this->create($formDados);

        if($retorno_produto){

            DB::commit();

            return [
                'sucesso'  => true,
                'mensagem' => 'Produto cadastrado com sucesso.'
            ];
        }

        DB::roolback();

        return [
            'sucesso'  => false,
            'mensagem' => 'Erro ao cadastrar novo produto.'
        ];
    }

    public function atualizarDados(Array $formDados): Array{

        DB::beginTransaction();

        //Atualiza dados do formulário
        $retorno_produto = $this->update($formDados);

        if($retorno_produto){

            DB::commit();

            return [
                'sucesso'  => true,
                'mensagem' => 'Produto atualizado com sucesso.'
            ];
        }

        DB::roolback();

        return [
            'sucesso'  => false,
            'mensagem' => 'Erro ao atualizar produto.'
        ];
    }
}
