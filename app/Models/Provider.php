<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Provider extends Model
{
    protected $fillable = ['name','cnpj'];
    

    public function novoFornecedor(Array $formDados): Array{

    	DB::beginTransaction();

    	//Cria o endereço com base nos dados do formulário
    	$retorno_endereco = Address::create($formDados);

    	//Cria o fornecedor com base nos dados do formulário
    	$retorno_fornecedor = $retorno_endereco->fornecedor()->create($formDados);

    	//Verifica se ambos foram inserido e  realiza commit, caso contrário realiza o roolback
    	if($retorno_endereco && $retorno_fornecedor){

    		DB::commit();

    		return [
    			'sucesso'  => true,
    			'mensagem' => 'Fornecedor cadastrado com sucesso.'
     		];
     	}

     	DB::roolback();

     	return [
     		'sucesso'  => false,
    		'mensagem' => 'Erro ao cadastrar novo fornecedor.'
     	];
    }

    public function atualizarDados(Array $formDados): Array{

        DB::beginTransaction();

        //Atualiza o fornecedor
        $retorno_fornecedor = $this->update($formDados);

        //Atualiza do Endereco
        $retorno_endereco = $this->endereco()->update($formDados);

        //Verifica se ambos foram atualizados e  realiza commit, caso contrário realiza o roolback
        if($retorno_endereco && $retorno_fornecedor){

            DB::commit();

            return [
                'sucesso'  => true,
                'mensagem' => 'Fornecedor atualizado com sucesso.'
            ];
        }

        DB::roolback();

        return [
            'sucesso'  => false,
            'mensagem' => 'Erro ao atualizado o fornecedor.'
        ];

    }

    //Função responável pelo OneToOne inverso com enredeço
    public function endereco(){
    	return $this->belongsTo(Address::class,"address_id");
    }

    //Função responável pelo ManyToOne inverso com produtos
    public function produtos(){
        return $this->hasMany(Product::class);
    }


}
