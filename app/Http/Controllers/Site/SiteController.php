<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product; //Produto

class SiteController extends Controller
{

	private $listaCompra = array();
	
    public function index(){
    	$produtos = Product::all();
    	return view('site/home/index',compact('produtos'));
    }

    public function carrinhoCompras($param){
    	
    	if($param == -1){
    		$produtos = Product::all();
    		return view('site/home/index',compact('produtos'));
    	}
    	//autencar

    	//Nova leção de dados
    	$produtos = collect();
    	//Converte de json para array
    	$array = json_decode($param);
    	//Monta a lista de produtos
    	$totalPedido = 0;
    	for ($i = 0; $i < count($array) ; $i++) {
		    //$key   = $keys[$i];
		    //$value = $array[$i];
		    $produto = Product::find($array[$i]);
		    $produtos->push($produto);
		    $totalPedido += $produto->sale_value;
		}

		$estados = ['Acre','Alagoas','Amapá','Amazonas','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Pará','Paraíba','Paraná','Pernambuco','Piauí','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondônia','Roraima','Santa Catarina','São Paulo','Sergipe','Tocantins'];

		return view('site/carrinho/carrinho-compras',compact('produtos','totalPedido','estados'));

		//dd($produtos);

    	
    	//return redirect()->route('login');
    	//dd($param);
    	//return response()->view('auth.login');
    	//return view('auth.login')->render();
    	//return response()->json($this->listaCompra);
    }

    public function carrinhoFinalizar(Request $request){
    	dd($request->all());
    }
}
