<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order; //Pedidos
use App\Models\Cart; //Carrinho
use Gate; 
use App\Http\Requests\PedidoCompraValidationFormRequest;
use App\Http\Requests\PedidoValidationFormRequest;

class PedidoController extends Controller
{

    private $pedido;

    public function __construct(Order $pedido){
        $this->pedido = $pedido;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Retorna todos os pedidos
        $pedidos = $this->pedido->all();

        return view('admin.pedido.listar-pedidos',compact('pedidos'));
    }

    public function pedidosPendentes()
    {
        //Retorna todos os pedidos
        $pedidos = $this->pedido->where('state','Aguardando')->get();
        return view('admin.pedido.listar-pedidos-pendentes',compact('pedidos'));
    }

    public function meusPedidos()
    {
        //Retorna todos os pedidos
        $pedidos = $this->pedido->where('user_id',auth()->user()->id)->get();
        return view('admin.meuspedidos.listar-pedidos',compact('pedidos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Carregamento Eager
        
        $pedido = $this->pedido->where('id',$id)->with(['endereco','usuario'])->get()->first();;
        $carrinhos = Cart::where('order_id',$id)->get();
        //dd($carrinhos);
        return view('admin.pedido.atualizar-pedido',compact('pedido','carrinhos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PedidoValidationFormRequest $request, $id)
    {
        $formDados = $request->all();

        $pedido = $this->pedido::find($id);

        $retorno = $pedido->atualizarSituacao($formDados);

        if($retorno['sucesso'])
            return redirect()
                    ->route('pedidos.index')
                    ->with('sucesso',$retorno['mensagem']);

        return redirect()
                    ->back()
                    ->with('error',$retorno['mensagem']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function carrinhoFinalizar(PedidoCompraValidationFormRequest $request){
        //antes verificar se campos preenchidos
        if(auth()->check()){
            $formDados = $request->all();
        
            $retorno = $this->pedido->novoPedido($formDados);

            if($retorno['sucesso'])
                return redirect()
                        ->route('meuspedidos')
                        ->with('sucesso',$retorno['mensagem']);

            return redirect()
                        ->back()
                        ->with('error',$retorno['mensagem']);
        }else{
            return redirect()->route('login');
        }
        
    }
}
