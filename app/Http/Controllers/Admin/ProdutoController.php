<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Provider; //Fornecedor
use App\Models\Category; //Categoria
use App\Models\Product; //Produto
use App\Http\Requests\ProdutoValidationFormRequest;

class ProdutoController extends Controller
{

    private $produto;

    public function __construct(Product $produto){
        $this->produto = $produto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = $this->produto->all();

        return view('admin.produto.listar-produtos',compact('produtos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fornecedores = Provider::all();
        $categorias = Category::all();
        return view('admin/produto/formulario-produto',compact('fornecedores','categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProdutoValidationFormRequest $request)
    {
        //dd($request->all());
        //dd($request->all());
        $formDados = $request->all();
        
        //if(!$request->fornecedores || $request->fornecedores[0] == null) //Verifica se algum registro de fornecedor foi informado
        //   $formDados['fornecedores'] = null;        

        $retorno = $this->produto->novoProduto($formDados);

        if($retorno['sucesso'])
            return redirect()
                    ->route('produtos.index')
                    ->with('sucesso',$retorno['mensagem']);

        return redirect()
                    ->back()
                    ->with('error',$retorno['mensagem']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Carregamento Eager 
        $produto = $this->produto->where('id',$id)->with(['fornecedor'],['categoria'])->get()->first();

        return view('admin/produto/dados-produto',compact('produto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fornecedores = Provider::all();
        $categorias = Category::all();

        //Carregamento Eager 
        $produto = $this->produto->where('id',$id)->with(['fornecedor'],['categoria'])->get()->first();

        //dd($produto->fornecedores);

        //dd($produtos);
        return view('admin/produto/formulario-produto',compact('fornecedores','produto','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProdutoValidationFormRequest $request, $id)
    {
        $formDados = $request->all();       

        //$this->fornecedor::find($id);
        $produto = $this->produto::find($id);

        $retorno = $produto->atualizarDados($formDados);

        if($retorno['sucesso'])
            return redirect()
                    ->route('produtos.index')
                    ->with('sucesso',$retorno['mensagem']);

        return redirect()
                    ->back()
                    ->with('error',$retorno['mensagem']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
