<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Provider; //Fornecedor
use App\Models\Address; //Endereço
use App\Http\Requests\FornecedorValidationFormRequest;
use Gate;

class FornecedorController extends Controller
{

    private $fornecedor;

    public function __construct(Provider $fornecedor){
        $this->fornecedor = $fornecedor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fornecedores = $this->fornecedor->all();

       // dd($fornecedores);


        return view('admin.fornecedor.listar-fornecedores',compact('fornecedores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $estados = ['Acre','Alagoas','Amapá','Amazonas','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Pará','Paraíba','Paraná','Pernambuco','Piauí','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondônia','Roraima','Santa Catarina','São Paulo','Sergipe','Tocantins'];
        return view('admin/fornecedor/formulario-fornecedor',compact('estados'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FornecedorValidationFormRequest $request)
    {
        $formDados = $request->all();

        $retorno = $this->fornecedor->novoFornecedor($formDados);

        if($retorno['sucesso'])
            return redirect()
                    ->route('fornecedores.index')
                    ->with('sucesso',$retorno['mensagem']);

        return redirect()
                    ->back()
                    ->with('error',$retorno['mensagem']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fornecedor = $this->fornecedor->where('id',$id)->with(['endereco'])->get()->first();

        return view('admin/fornecedor/dados-fornecedor',compact('fornecedor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Carregamento Eager 
        $fornecedor = $this->fornecedor->where('id',$id)->with(['endereco'])->get()->first();
        
        $estados = ['Acre','Alagoas','Amapá','Amazonas','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Pará','Paraíba','Paraná','Pernambuco','Piauí','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondônia','Roraima','Santa Catarina','São Paulo','Sergipe','Tocantins'];

        return view('admin/fornecedor/formulario-fornecedor', compact('estados', 'fornecedor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FornecedorValidationFormRequest $request, $id)
    {
        $dadosForm = $request->all();
        // $fornecedor = $this->fornecedor->where('id',$id)->with(['endereco'])->get()->first();
        $fornecedor = $this->fornecedor::find($id);

        $retorno = $fornecedor->atualizarDados($dadosForm);

        if($retorno['sucesso'])
            return redirect()
                    ->route('fornecedores.index')
                    ->with('sucesso',$retorno['mensagem']);

        return redirect()
                    ->back()
                    ->with('error',$retorno['mensagem']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
