<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Provider; //Fornecedor
use App\Models\Product; //Fornecedor
use App\User; //Fornecedor
use App\Models\Order; //Fornecedor

class AdminController extends Controller
{
    public function index(){
    	$totalFornecedor = Provider::all()->count();
    	$totalProdutos = Product::all()->count();
    	$totalUsuarios = User::where('level','cliente')->count();
    	$totalPedidos = Order::all()->count() ;
    	$pedidosAbertos = Order::where('state','Aguardando')	->count() ;
    	$meusPedidos = Order::where('user_id',auth()->user()->id)->count() ;
    	//return view('admin/home/index', compact('totalFornecedor', 'totalProdutos', 'totalUsuarios', 'totalPedidos'));
    	return view('admin.home.index',compact('totalFornecedor', 'totalProdutos', 'totalUsuarios', 'totalPedidos','pedidosAbertos','meusPedidos'));
    }
}